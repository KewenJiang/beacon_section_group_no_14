package com.cs425.web.utils;

import com.cs425.web.exception.SysException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DBUtil {
    /**
     * 加载驱动，创建连接
     *
     * @return
     */
    public Connection getConn() {
        Connection conn = null;
        // 驱动
        String driver = "org.postgresql.Driver";
        // 连接地址
        String url = "jdbc:postgresql://localhost:5432/postgres";
        // 连接用户名
        String username = "postgres";
        // 密码
        String password = "root";
        try {
            Class.forName(driver);
            conn = DriverManager.getConnection(url, username, password);
            System.out.println(conn.isClosed());

        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return conn;

    }



    /**
     * 执行数据的更改：包括了增加、修改、删除
     *
     * @param sql
     * @param obj
     * @return
     */
    public int execUpdate(String sql, Object[] obj) throws SysException {
        System.out.println("execsql====" + sql);

        Connection conn = this.getConn();

        if (conn == null)
            throw new SysException(100, "系统故障，请联系管理员");
        // 预编译处理对象 ： 防止SQL注入 攻击
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement(sql);

            // 有参数
            if (obj != null) {
                // 循环设置参数
                for (int i = 0; i < obj.length; i++) {
                    ps.setObject(i + 1, obj[i]);
                }
            }

            return ps.executeUpdate();

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            throw new SysException(100, "系统故障，请联系管理员");

        }
        /**
         * 关闭数据库资源
         */
        finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                //如果采用数据库连接池则此处不是断掉连接，而是归还连接
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

    }

    /**
     * 执行数据的查询，把ResultSet封装到List<Map>中
     *
     * @param sql
     * @param obj
     * @return
     * @throws SysException
     */
    public List<Map<String, Object>> exexQuery(String sql, Object[] obj)
            throws SysException {
        System.out.println("querysql====" + sql);

        List<Map<String, Object>> list = new ArrayList<>();

        PreparedStatement ps = null;

        Connection conn = this.getConn();
        // 结果集 不能当对象返回
        ResultSet rs = null;

        if (conn == null)
            throw new SysException(100, "系统故障，请联系管理员");

        try {
            ps = conn.prepareStatement(sql);

            // 设置参数
            if (obj != null) {
                for (int i = 0; i < obj.length; i++) {
                    ps.setObject(i + 1, obj[i]);
                }
            }
            // 执行SQL返回结果集
            rs = ps.executeQuery();
            // 获得结果集结构
            ResultSetMetaData rsmd = rs.getMetaData();

            // 如果存在下一条
            while (rs.next()) {
                // 一条数据
                Map<String, Object> map = new HashMap<>();

                // 从第一个字段到最后一个字段
                for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                    // key=字段名 value=字段值
                    map.put(rsmd.getColumnName(i), rs.getObject(i));
                }
                // 添加到list中
                list.add(map);

            }

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            throw new SysException(100, "系统故障，请联系管理员");
        }
        /**
         * 依次关闭资源
         */
        finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        return list;
    }

    public static void main(String[] args) {
        DBUtil db = new DBUtil();
        db.getConn();

    }
}
