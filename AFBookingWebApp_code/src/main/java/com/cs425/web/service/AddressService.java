package com.cs425.web.service;

import com.cs425.web.bean.AddressBean;
import com.cs425.web.dao.AddressDao;
import com.cs425.web.exception.AppException;
import com.cs425.web.exception.SysException;

public class AddressService {
    AddressDao dao =  new AddressDao();

    public int addAddress(AddressBean address)throws AppException {
        try {
            return dao.addAddress(address);
        } catch (SysException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            throw new AppException(200, "系统故障，请联系管理员");
        }
    }

    public AddressBean getOne(String email,Integer postalCode) throws AppException{
        try {
            return dao.querybyEmAndCode(email,postalCode);
        } catch (SysException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            throw new AppException(200, "系统故障，请联系管理员");
        }
    }

    public int updateAddress(AddressBean address)throws AppException{
        try {
            int upAddressCount = dao.updateAddressByEm(address);
            return upAddressCount;
        } catch (SysException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            throw new AppException(200, "系统故障，请联系管理员");
        }
    }

    public int deleteAddress(String email,Integer postalCode)throws AppException{
        try {
            return dao.deleteAddressByEm(email,postalCode);
        } catch (SysException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            throw new AppException(200, "系统故障，请联系管理员");
        }
    }
}
