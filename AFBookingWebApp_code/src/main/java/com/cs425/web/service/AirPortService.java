package com.cs425.web.service;

import com.cs425.web.bean.AirPortBean;
import com.cs425.web.bean.AirlineBean;
import com.cs425.web.dao.AirPortDao;
import com.cs425.web.dao.AirlineDao;
import com.cs425.web.exception.AppException;
import com.cs425.web.exception.SysException;

public class AirPortService {
    AirPortDao dao =  new AirPortDao();

    public int addAirPort(AirPortBean airport)throws AppException {
        try {
            return dao.addAirport(airport);
        } catch (SysException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            throw new AppException(200, "系统故障，请联系管理员");
        }
    }

    public AirPortBean getOne(String iata) throws AppException{
        try {
            return dao.querybyiata(iata);
        } catch (SysException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            throw new AppException(200, "系统故障，请联系管理员");
        }
    }

    public int updateAirPort(AirPortBean airport)throws AppException{
        try {
            int upAddressCount = dao.updateAirport(airport);
            return upAddressCount;
        } catch (SysException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            throw new AppException(200, "系统故障，请联系管理员");
        }
    }

    public int deleteAirPort(String iata)throws AppException{
        try {
            return dao.deleteAirPort(iata);
        } catch (SysException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            throw new AppException(200, "系统故障，请联系管理员");
        }
    }
}
