package com.cs425.web.service;

import com.cs425.web.bean.AddressBean;
import com.cs425.web.bean.FlightBean;
import com.cs425.web.dao.AddressDao;
import com.cs425.web.dao.FlightDao;
import com.cs425.web.exception.AppException;
import com.cs425.web.exception.SysException;

public class FlightService {
    FlightDao dao =  new FlightDao();

    public int BookFlight(FlightBean flightBean)throws AppException {
        try {
            return dao.addFlight(flightBean);
        } catch (SysException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            throw new AppException(200, "系统故障，请联系管理员");
        }
    }

    public FlightBean getOne(String departureAirport,String destinationAirport, Double firstClassPrice,Double economyClassPrice) throws AppException{
        try {
            return dao.querybyDepAndPrice(departureAirport,destinationAirport,firstClassPrice,economyClassPrice);
        } catch (SysException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            throw new AppException(200, "系统故障，请联系管理员");
        }
    }

    public int updateAddressByEm(FlightBean flight)throws AppException{
        try {
            int uCount = dao.updateFlightbyNo(flight);
            return uCount;
        } catch (SysException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            throw new AppException(200, "系统故障，请联系管理员");
        }
    }

}
