package com.cs425.web.service;

import com.cs425.web.bean.AddressBean;
import com.cs425.web.bean.AirlineBean;
import com.cs425.web.dao.AddressDao;
import com.cs425.web.dao.AirlineDao;
import com.cs425.web.exception.AppException;
import com.cs425.web.exception.SysException;

public class AirLineService {
    AirlineDao dao =  new AirlineDao();

    public int addAirline(AirlineBean airline)throws AppException {
        try {
            return dao.addAirline(airline);
        } catch (SysException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            throw new AppException(200, "系统故障，请联系管理员");
        }
    }

    public AirlineBean getOne(String airLineId) throws AppException{
        try {
            return dao.querybyAirLineId(airLineId);
        } catch (SysException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            throw new AppException(200, "系统故障，请联系管理员");
        }
    }

    public int updateAirline(AirlineBean airline)throws AppException{
        try {
            int upAddressCount = dao.updateAirline(airline);
            return upAddressCount;
        } catch (SysException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            throw new AppException(200, "系统故障，请联系管理员");
        }
    }

    public int deleteAirline(String airLineId)throws AppException{
        try {
            return dao.deleteAirlineById(airLineId);
        } catch (SysException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            throw new AppException(200, "系统故障，请联系管理员");
        }
    }
}
