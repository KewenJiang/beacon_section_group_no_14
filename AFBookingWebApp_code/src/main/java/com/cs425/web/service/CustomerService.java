package com.cs425.web.service;

import com.cs425.web.bean.CustomerBean;
import com.cs425.web.dao.CustomerDao;
import com.cs425.web.exception.AppException;
import com.cs425.web.exception.SysException;

public class CustomerService {
  CustomerDao dao =  new CustomerDao();

    public CustomerBean chkLogin(String email,String phoneNumber)throws AppException{
        try {
            return dao.queryByLogin(email, phoneNumber);
        } catch (SysException e) {
            // TODO Auto-generated catch block
            //系统故障
            if(e.getErrcode()==100) {
                throw new AppException(200, "系统故障，请联系管理员");
            }
            else {
                throw new AppException(201, "邮箱地址或手机号码输入有误，请重新输入!!");
            }
        }

    }

    public int addCustomer(CustomerBean customer)throws AppException {
        try {
            return dao.addCustomer(customer);
        } catch (SysException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            throw new AppException(200, "系统故障，请联系管理员");
        }
    }

    public CustomerBean getOne(String email) throws AppException{
        try {
            return dao.querybyEm(email);
        } catch (SysException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            throw new AppException(200, "系统故障，请联系管理员");
        }
    }

    public int updateCustomer(CustomerBean customer)throws AppException{
        try {
             int upCustomerCount = dao.updateCustomer(customer);
            return upCustomerCount;
        } catch (SysException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            throw new AppException(200, "系统故障，请联系管理员");
        }
    }

    public int deleteCustomer(String email)throws AppException{
        try {
            return dao.deleteCustomerByEm(email);
        } catch (SysException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            throw new AppException(200, "系统故障，请联系管理员");
        }
    }

}
