package com.cs425.web.service;

import com.cs425.web.bean.CreditCardsBean;
import com.cs425.web.dao.CreditCardsDao;
import com.cs425.web.exception.AppException;
import com.cs425.web.exception.SysException;

public class CreditCardsService {
    CreditCardsDao dao =  new CreditCardsDao();

    public int addCreditCards(CreditCardsBean credit)throws AppException {
        try {
            return dao.addCreditCards(credit);
        } catch (SysException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            throw new AppException(200, "系统故障，请联系管理员");
        }
    }

    public CreditCardsBean getOne(String email,String cardNumber) throws AppException{
        try {
            return dao.querybyEmAndCard(email,cardNumber);
        } catch (SysException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            throw new AppException(200, "系统故障，请联系管理员");
        }
    }

    public int updateCreditCards(CreditCardsBean credit)throws AppException{
        try {
            int upAddressCount = dao.updateCreditCardsByEm(credit);
            return upAddressCount;
        } catch (SysException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            throw new AppException(200, "系统故障，请联系管理员");
        }
    }

    public int deleteCreditCards(String email,String cardNumber)throws AppException{
        try {
            return dao.deleteCreditCardsByEm(email,cardNumber);
        } catch (SysException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            throw new AppException(200, "系统故障，请联系管理员");
        }
    }
}
