package com.cs425.web.dao;

import com.cs425.web.bean.AddressBean;
import com.cs425.web.bean.CustomerBean;
import com.cs425.web.exception.SysException;
import com.cs425.web.utils.DBUtil;

import java.util.Date;
import java.util.List;
import java.util.Map;

public class AddressDao {

    DBUtil db = new DBUtil();

    public int addAddress(AddressBean address) throws SysException {
        String sql = "insert into address(state,city,area,street_name,apt_name,house_number,postal_code,email_address) values(?,?,?,?,?,?,?,?)";

        Object[] args = { address.getState(), address.getCity(), address.getArea(), address.getStreetName(),address.getAptName(),address.getHouseNumber(),address.getPostalCode(),address.getEmailAddress() };

        return db.execUpdate(sql, args);

    }

    public int updateAddressByEm(AddressBean address) throws SysException {
        String sql = "update address set state=?,city=?,area=?,street_name=?,apt_name=?,house_number=?,postal_code where email_address=?";

        Object[] args = { address.getState(), address.getCity(), address.getArea(), address.getStreetName(),address.getAptName(),address.getHouseNumber(),address.getPostalCode(),address.getEmailAddress() };

        return db.execUpdate(sql, args);

    }

    public AddressBean querybyEmAndCode(String email,Integer postalCode) throws SysException {
        String sql = "select * from address  where email_address = ? and postal_code = ?";
        Object[] args = { email,postalCode };
        List<Map<String, Object>> list = db.exexQuery(sql, args);

        if (list == null || list.size() != 1) {
            throw new SysException(101, "获得数据失败");
        }

        return mapToAddress(list.get(0));

    }

    public int deleteAddressByEm(String email,Integer postalCode)throws SysException{
        String sql = "delete from address  where email_address = ? and postal_code =?";
        Object[] args = { email,postalCode };
        return db.execUpdate(sql, args);
    }


    private AddressBean mapToAddress(Map<String, Object> map) {
        AddressBean ads = new AddressBean();
        ads.setState((String) map.get("state"));
        ads.setCity((String) map.get("city"));
        ads.setArea((String) map.get("area"));
        ads.setStreetName((String) map.get("street_name"));
        ads.setAptName((String) map.get("apt_name"));
        ads.setHouseNumber((String) map.get("house_number"));
        ads.setPostalCode((Integer) map.get("postal_code"));
        ads.setEmailAddress((String) map.get("email_address"));
        return ads;
    }
}
