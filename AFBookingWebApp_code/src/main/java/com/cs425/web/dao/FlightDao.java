package com.cs425.web.dao;

import com.cs425.web.bean.AddressBean;
import com.cs425.web.bean.FlightBean;
import com.cs425.web.exception.SysException;
import com.cs425.web.utils.DBUtil;

import java.util.List;
import java.util.Map;

public class FlightDao {

    DBUtil db = new DBUtil();

    public int addFlight(FlightBean flight) throws SysException {
        String sql = "insert into flight(airline_id,flight_number,flight_data,departure_airport,destination_airport,departure_time,destination_time,flight_length,first_class_seats,economy_class_seats,first_class_price,economy_class_price) values(?,?,?,?,?,?,?,?)";

        Object[] args = { flight.getAirlineId(), flight.getFlightNumber(), flight.getFlightData(), flight.getDepartureAirport(),flight.getDestinationAirport(),flight.getDepartureTime(),flight.getDestinationTime(),flight.getFlightLength(),flight.getFirstClassSeats(),flight.getEconomyClassSeats(),flight.getFirstClassPrice(),flight.getEconomyClassPrice() };

        return db.execUpdate(sql, args);

    }

    public int updateFlightbyNo(FlightBean flight) throws SysException {
        String sql = "update flight set airline_id=?,flight_data=?,departure_airport=?,destination_airport=?,departure_time=?,destination_time=?,flight_length=?,first_class_seats=?,economy_class_seats=?,first_class_price=?,economy_class_price=? where flight_number=?";

        Object[] args = { flight.getAirlineId(), flight.getFlightData(), flight.getDepartureAirport(),flight.getDestinationAirport(),flight.getDepartureTime(),flight.getDestinationTime(),flight.getFirstClassSeats(),flight.getEconomyClassSeats(),flight.getFirstClassPrice(),flight.getEconomyClassPrice(),flight.getFlightNumber() };

        return db.execUpdate(sql, args);

    }

    public FlightBean querybyDepAndPrice(String departureAirport,String destinationAirport, Double firstClassPrice,Double economyClassPrice) throws SysException {
        String sql = "select * from flight  where departure_airport = ? and destination_airport = ? or first_class_price = ? or economy_class_price =?";
        Object[] args = { departureAirport,destinationAirport,firstClassPrice,economyClassPrice };
        List<Map<String, Object>> list = db.exexQuery(sql, args);

        if (list == null || list.size() != 1) {
            throw new SysException(101, "获得数据失败");
        }

        return mapToFlight(list.get(list.size()));

    }



    private FlightBean mapToFlight(Map<String, Object> map) {
        FlightBean fl = new FlightBean();
        fl.setAirlineId((String) map.get("airlineId"));
        fl.setFlightNumber((String) map.get("flightNumber"));
        fl.setFlightData((String) map.get("flightData"));
        fl.setDepartureAirport((String) map.get("departureAirport"));
        fl.setDestinationAirport((String) map.get("destinationAirport"));
        fl.setDepartureTime((String) map.get("departureTime"));
        fl.setDestinationTime((String) map.get("destinationTime"));
        fl.setFlightLength((Integer) map.get("flightLength"));
        fl.setFirstClassSeats((Integer) map.get("firstClassSeats"));
        fl.setEconomyClassSeats((Integer) map.get("economyClassSeats"));
        fl.setFirstClassPrice((Double) map.get("firstClassPrice"));
        fl.setEconomyClassPrice((Double) map.get("economyClassPrice"));
        return fl;
    }
}
