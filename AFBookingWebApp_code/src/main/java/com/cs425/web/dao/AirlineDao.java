package com.cs425.web.dao;

import com.cs425.web.bean.AddressBean;
import com.cs425.web.bean.AirlineBean;
import com.cs425.web.exception.SysException;
import com.cs425.web.utils.DBUtil;

import java.util.List;
import java.util.Map;

public class AirlineDao {

    DBUtil db = new DBUtil();

    public int addAirline(AirlineBean airline) throws SysException {
        String sql = "insert into airline(airline_id,airline_name,origin_country) values(?,?,?)";

        Object[] args = { airline.getAirlineId(), airline.getAirlineName(), airline.getOriginCuntry() };

        return db.execUpdate(sql, args);

    }

    public int updateAirline(AirlineBean airline) throws SysException {
        String sql = "update airline set airline_name=?,origin_country=? where airline_id=?";

        Object[] args = { airline.getAirlineName(), airline.getOriginCuntry(), airline.getAirlineId() };

        return db.execUpdate(sql, args);

    }

        public AirlineBean querybyAirLineId(String airlindId) throws SysException {
        String sql = "select * from airline  where airline_id = ?";
        Object[] args = { airlindId };
        List<Map<String, Object>> list = db.exexQuery(sql, args);

        if (list == null || list.size() != 1) {
            throw new SysException(101, "获得数据失败");
        }

        return mapToAirline(list.get(0));

    }

    public int deleteAirlineById(String airlindId)throws SysException{
        String sql = "delete from airline  where airline_id = ?";
        Object[] args = { airlindId };
        return db.execUpdate(sql, args);
    }


    private AirlineBean mapToAirline(Map<String, Object> map) {
        AirlineBean al = new AirlineBean();
        al.setAirlineId((String) map.get("airlindId"));
        al.setAirlineName((String) map.get("airlineName"));
        al.setOriginCuntry((String) map.get("originCuntry"));
        return al;
    }
}
