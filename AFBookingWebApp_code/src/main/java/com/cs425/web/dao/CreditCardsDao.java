package com.cs425.web.dao;

import com.cs425.web.bean.AddressBean;
import com.cs425.web.bean.CreditCardsBean;
import com.cs425.web.exception.SysException;
import com.cs425.web.utils.DBUtil;

import java.util.List;
import java.util.Map;

public class CreditCardsDao {

    DBUtil db = new DBUtil();

    public int addCreditCards(CreditCardsBean credit) throws SysException {
        String sql = "insert into credit_cards(card_number,bank_name,address_id,email_address) values(?,?,?,?)";

        Object[] args = { credit.getCardNumber(),credit.getBankName(),credit.getAddressId(),credit.getEmailAddress() };

        return db.execUpdate(sql, args);

    }

    public int updateCreditCardsByEm(CreditCardsBean credit) throws SysException {
        String sql = "update credit_cards set card_number=?,bank_name=?,address_id=? where email_address=?";

        Object[] args = { credit.getCardNumber(),credit.getBankName(),credit.getAddressId(),credit.getEmailAddress() };

        return db.execUpdate(sql, args);

    }

    public CreditCardsBean querybyEmAndCard(String email,String cardNumber) throws SysException {
        String sql = "select * from credit_cards  where email_address = ? and card_number = ?";
        Object[] args = { email,cardNumber };
        List<Map<String, Object>> list = db.exexQuery(sql, args);

        if (list == null || list.size() != 1) {
            throw new SysException(101, "获得数据失败");
        }

        return mapToCreditCards(list.get(0));

    }

    public int deleteCreditCardsByEm(String email,String cardNumber)throws SysException{
        String sql = "delete from credit_cards  where email_address = ? and card_number = ?";
        Object[] args = { email, cardNumber};
        return db.execUpdate(sql, args);
    }


    private CreditCardsBean mapToCreditCards(Map<String, Object> map) {
        CreditCardsBean cds = new CreditCardsBean();
        cds.setCardsId((String) map.get("cards_id"));
        cds.setCardNumber((String) map.get("card_number"));
        cds.setBankName((String) map.get("bank_name"));
        cds.setAddressId((String) map.get("address_id"));
        cds.setEmailAddress((String) map.get("email_address"));
        return cds;
    }
}
