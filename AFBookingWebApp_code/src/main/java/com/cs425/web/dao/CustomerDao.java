package com.cs425.web.dao;

import com.cs425.web.bean.CustomerBean;
import com.cs425.web.exception.SysException;
import com.cs425.web.utils.DBUtil;

import java.util.Date;
import java.util.List;
import java.util.Map;

public class CustomerDao {
    DBUtil db = new DBUtil();

    public CustomerBean queryByLogin(String email,String phoneNumber) throws SysException{
        String sql="select * from customer where email_address=? and phone_number=?";

        Object[] args= {email,phoneNumber};


        List<Map<String, Object>>list=db.exexQuery(sql, args);

        if(list==null || list.size()!=1) {
            throw new SysException(101, "用户登录失败");
        }
        CustomerBean customer=new CustomerBean();
        //获得唯一一个
        Map customerMap=list.get(0);

        customer.setEmailAddress((String)customerMap.get("email_address"));
        customer.setCustomerName((String)customerMap.get("customer_name"));
        customer.setPhoneNumber((String)customerMap.get("phone_number"));
        customer.setLastLoginTime((String) customerMap.get("last_login_time"));
        customer.setIATA((String)customerMap.get("iata"));
        return customer;
    }

    public int addCustomer(CustomerBean customer) throws SysException {
        String sql = "insert into customer(email_address,customer_name,phone_number,last_login_time,iata) values(?,?,?,?,?)";

        Object[] args = { customer.getEmailAddress(), customer.getCustomerName(), customer.getPhoneNumber(), customer.getLastLoginTime(),customer.getIATA() };

        return db.execUpdate(sql, args);

    }

    public int updateCustomer(CustomerBean customer) throws SysException {
        String sql = "update customer set customer_name=?,phone_number=?,last_login_time=?,iata=? where email_address=?";

        Object[] args = { customer.getCustomerName(), customer.getPhoneNumber(), customer.getLastLoginTime(), customer.getIATA(), customer.getEmailAddress() };

        return db.execUpdate(sql, args);

    }

    public CustomerBean querybyEm(String email) throws SysException {
        String sql = "select * from customer  where email_address=?";
        Object[] args = { email };
        List<Map<String, Object>> list = db.exexQuery(sql, args);

        if (list == null || list.size() != 1) {
            throw new SysException(101, "获得数据失败");
        }

        return mapToCustomers(list.get(0));

    }

    public int deleteCustomerByEm(String email)throws SysException{
        String sql = "delete from customer  where email_address=?";
        Object[] args = { email };
        return db.execUpdate(sql, args);
    }


    private CustomerBean mapToCustomers(Map<String, Object> map) {
        CustomerBean cst = new CustomerBean();
        cst.setEmailAddress((String) map.get("email_address"));
        cst.setCustomerName((String) map.get("customer_name"));
        cst.setPhoneNumber((String) map.get("phone_number"));
        cst.setLastLoginTime((String) map.get("last_login_time"));
        cst.setIATA((String) map.get("iata"));
        return cst;
    }
}
