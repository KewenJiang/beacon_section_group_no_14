package com.cs425.web.dao;

import com.cs425.web.bean.AirPortBean;
import com.cs425.web.bean.AirlineBean;
import com.cs425.web.exception.SysException;
import com.cs425.web.utils.DBUtil;

import java.util.List;
import java.util.Map;

public class AirPortDao {

    DBUtil db = new DBUtil();

    public int addAirport(AirPortBean airport) throws SysException {
        String sql = "insert into airport(iata,country,state) values(?,?,?)";

        Object[] args = { airport.getIata(), airport.getCountry(), airport.getState() };

        return db.execUpdate(sql, args);

    }

    public int updateAirport(AirPortBean airport) throws SysException {
        String sql = "update airport set country=?,state=? where iata=?";

        Object[] args = { airport.getCountry(), airport.getState(), airport.getIata() };

        return db.execUpdate(sql, args);

    }

        public AirPortBean querybyiata(String iata) throws SysException {
        String sql = "select * from airport  where iata = ?";
        Object[] args = { iata };
        List<Map<String, Object>> list = db.exexQuery(sql, args);

        if (list == null || list.size() != 1) {
            throw new SysException(101, "获得数据失败");
        }

        return mapToAirPort(list.get(0));

    }

    public int deleteAirPort(String iata)throws SysException{
        String sql = "delete from airport  where iata = ?";
        Object[] args = { iata };
        return db.execUpdate(sql, args);
    }


    private AirPortBean mapToAirPort(Map<String, Object> map) {
        AirPortBean ap = new AirPortBean();
        ap.setIata((String) map.get("iata"));
        ap.setCountry((String) map.get("country"));
        ap.setState((String) map.get("state"));
        return ap;
    }
}
