package com.cs425.web.exception;

public class AppException extends Exception{
    private int errcode;
    private String errMsg;

    public AppException(int errcode, String errMsg) {
        this.errcode = errcode;
        this.errMsg = errMsg;
    }

    public int getErrcode() {
        return errcode;
    }

    public void setErrcode(int errcode) {
        this.errcode = errcode;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

}
