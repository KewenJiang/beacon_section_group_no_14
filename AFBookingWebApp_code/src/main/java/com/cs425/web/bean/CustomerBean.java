package com.cs425.web.bean;

import java.util.Date;

public class CustomerBean {
    private String emailAddress;
    private String customerName;
    private String phoneNumber;
    private String lastLoginTime;
    private String IATA;

    public CustomerBean(){

    }

    public CustomerBean(String emailAddress, String customerName, String phoneNumber, String lastLoginTime,String IATA) {
        this.emailAddress = emailAddress;
        this.customerName = customerName;
        this.phoneNumber = phoneNumber;
        this.lastLoginTime = lastLoginTime;
        this.IATA = IATA;
    }


    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(String lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public String getIATA() {
        return IATA;
    }

    public void setIATA(String IATA) {
        this.IATA = IATA;
    }

    @Override
    public String toString() {
        return "CustomerBean{" +
                "emailAddress='" + emailAddress + '\'' +
                ", name='" + customerName + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", lastLoginTime=" + lastLoginTime +
                ", IATA='" + IATA + '\'' +
                '}';
    }
}

