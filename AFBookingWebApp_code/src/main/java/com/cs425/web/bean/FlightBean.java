package com.cs425.web.bean;

public class FlightBean {
    private String airlineId;
    private String flightNumber;
    private String flightData;
    private String departureAirport;
    private String destinationAirport;
    private String departureTime;
    private String destinationTime;
    private Integer flightLength;
    private Integer firstClassSeats;
    private Integer economyClassSeats;
    private Double firstClassPrice;
    private Double economyClassPrice;

    public FlightBean(String airlineId, String flightNumber, String flightData, String departureAirport, String destinationAirport, String departureTime, String destinationTime, Integer flightLength, Integer firstClassSeats, Integer economyClassSeats, Double firstClassPrice, Double economyClassPrice) {
        this.airlineId = airlineId;
        this.flightNumber = flightNumber;
        this.flightData = flightData;
        this.departureAirport = departureAirport;
        this.destinationAirport = destinationAirport;
        this.departureTime = departureTime;
        this.destinationTime = destinationTime;
        this.flightLength = flightLength;
        this.firstClassSeats = firstClassSeats;
        this.economyClassSeats = economyClassSeats;
        this.firstClassPrice = firstClassPrice;
        this.economyClassPrice = economyClassPrice;
    }

    public FlightBean(){

    }

    public String getAirlineId() {
        return airlineId;
    }

    public void setAirlineId(String airlineId) {
        this.airlineId = airlineId;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getFlightData() {
        return flightData;
    }

    public void setFlightData(String flightData) {
        this.flightData = flightData;
    }

    public String getDepartureAirport() {
        return departureAirport;
    }

    public void setDepartureAirport(String departureAirport) {
        this.departureAirport = departureAirport;
    }

    public String getDestinationAirport() {
        return destinationAirport;
    }

    public void setDestinationAirport(String destinationAirport) {
        this.destinationAirport = destinationAirport;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    public String getDestinationTime() {
        return destinationTime;
    }

    public void setDestinationTime(String destinationTime) {
        this.destinationTime = destinationTime;
    }

    public Integer getFlightLength() {
        return flightLength;
    }

    public void setFlightLength(Integer flightLength) {
        this.flightLength = flightLength;
    }

    public Integer getFirstClassSeats() {
        return firstClassSeats;
    }

    public void setFirstClassSeats(Integer firstClassSeats) {
        this.firstClassSeats = firstClassSeats;
    }

    public Integer getEconomyClassSeats() {
        return economyClassSeats;
    }

    public void setEconomyClassSeats(Integer economyClassSeats) {
        this.economyClassSeats = economyClassSeats;
    }

    public Double getFirstClassPrice() {
        return firstClassPrice;
    }

    public void setFirstClassPrice(Double firstClassPrice) {
        this.firstClassPrice = firstClassPrice;
    }

    public Double getEconomyClassPrice() {
        return economyClassPrice;
    }

    public void setEconomyClassPrice(Double economyClassPrice) {
        this.economyClassPrice = economyClassPrice;
    }

    @Override
    public String toString() {
        return "FlightBean{" +
                "airlineId='" + airlineId + '\'' +
                ", flightNumber='" + flightNumber + '\'' +
                ", flightData='" + flightData + '\'' +
                ", departureAirport='" + departureAirport + '\'' +
                ", destinationAirport='" + destinationAirport + '\'' +
                ", departureTime='" + departureTime + '\'' +
                ", destinationTime='" + destinationTime + '\'' +
                ", flightLength=" + flightLength +
                ", firstClassSeats=" + firstClassSeats +
                ", economyClassSeats=" + economyClassSeats +
                ", firstClassPrice=" + firstClassPrice +
                ", economyClassPrice=" + economyClassPrice +
                '}';
    }
}
