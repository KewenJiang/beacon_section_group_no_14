package com.cs425.web.bean;

public class CreditCardsBean {
    private String cardsId;
    private String cardNumber;
    private String bankName;
    private String addressId;
    private String emailAddress;

    public CreditCardsBean( String cardNumber, String bankName, String addressId, String emailAddress) {
        this.cardNumber = cardNumber;
        this.bankName = bankName;
        this.addressId = addressId;
        this.emailAddress = emailAddress;
    }

    public CreditCardsBean(){

    }

    public String getCardsId() {
        return cardsId;
    }

    public void setCardsId(String cardsId) {
        this.cardsId = cardsId;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getAddressId() {
        return addressId;
    }

    public void setAddressId(String addressId) {
        this.addressId = addressId;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    @Override
    public String toString() {
        return "CreditCardsBean{" +
                "cardsId='" + cardsId + '\'' +
                ", cardNumber='" + cardNumber + '\'' +
                ", bankName='" + bankName + '\'' +
                ", addressId='" + addressId + '\'' +
                ", emailAddress='" + emailAddress + '\'' +
                '}';
    }
}
