package com.cs425.web.bean;



public class AirPortBean {
    private String iata;
    private String country;
    private String state;

    public AirPortBean(String iata, String country, String state) {
        this.iata = iata;
        this.country = country;
        this.state = state;
    }

    public AirPortBean(){

    }

    public String getIata() {
        return iata;
    }

    public void setIata(String iata) {
        this.iata = iata;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "AirPortBean{" +
                "iata='" + iata + '\'' +
                ", country='" + country + '\'' +
                ", state='" + state + '\'' +
                '}';
    }
}
