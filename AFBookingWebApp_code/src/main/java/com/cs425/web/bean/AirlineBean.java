package com.cs425.web.bean;



public class AirlineBean {
    private String airlineId;
    private String airlineName;
    private String originCuntry;

    public AirlineBean(String airlineId, String airlineName, String originCuntry) {
        this.airlineId = airlineId;
        this.airlineName = airlineName;
        this.originCuntry = originCuntry;
    }

    public AirlineBean(){

    }


    public String getAirlineId() {
        return airlineId;
    }

    public void setAirlineId(String airlineId) {
        this.airlineId = airlineId;
    }

    public String getAirlineName() {
        return airlineName;
    }

    public void setAirlineName(String airlineName) {
        this.airlineName = airlineName;
    }

    public String getOriginCuntry() {
        return originCuntry;
    }

    public void setOriginCuntry(String originCuntry) {
        this.originCuntry = originCuntry;
    }

    @Override
    public String toString() {
        return "AirlineBean{" +
                "airlineId='" + airlineId + '\'' +
                ", airlineName='" + airlineName + '\'' +
                ", originCuntry='" + originCuntry + '\'' +
                '}';
    }
}
