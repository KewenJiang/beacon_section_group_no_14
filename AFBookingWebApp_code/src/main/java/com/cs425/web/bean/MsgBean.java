package com.cs425.web.bean;

public class MsgBean {
    private int status; // 状态
    private String errmsg; //错误信息

    public void setStatus(int status) {
        this.status=status;
    }
    public int getStatus() {
        return this.status;
    }

    public void setErrmsg(String errmsg) {
        this.errmsg=errmsg;
    }
    public String getErrmsg() {
        return this.errmsg;
    }
}
