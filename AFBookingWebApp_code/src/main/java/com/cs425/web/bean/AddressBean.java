package com.cs425.web.bean;



public class AddressBean {
    private String addressId;
    private String state;
    private String city;
    private String area;
    private String streetName;
    private String aptName;
    private String houseNumber;
    private Integer postalCode;
    private String emailAddress;

    public AddressBean( String state, String city, String area, String streetName, String aptName, String houseNumber, Integer postalCode,String emailAddress) {
        this.state = state;
        this.city = city;
        this.area = area;
        this.streetName = streetName;
        this.aptName = aptName;
        this.houseNumber = houseNumber;
        this.postalCode = postalCode;
        this.emailAddress = emailAddress;
    }

    public AddressBean(){

    }

    public String getAddressId() {
        return addressId;
    }

    public void setAddressId(String addressId) {
        this.addressId = addressId;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getAptName() {
        return aptName;
    }

    public void setAptName(String aptName) {
        this.aptName = aptName;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public Integer getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(Integer postalCode) {
        this.postalCode = postalCode;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    @Override
    public String toString() {
        return "AddressBean{" +
                "addressId='" + addressId + '\'' +
                ", state='" + state + '\'' +
                ", city='" + city + '\'' +
                ", area='" + area + '\'' +
                ", streetName='" + streetName + '\'' +
                ", aptName='" + aptName + '\'' +
                ", houseNumber='" + houseNumber + '\'' +
                ", postalCode=" + postalCode +
                ", emailAddress='" + emailAddress + '\'' +
                '}';
    }
}
