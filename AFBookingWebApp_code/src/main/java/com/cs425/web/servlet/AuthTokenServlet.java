package com.cs425.web.servlet;

import com.auth0.jwt.JWT;
import com.cs425.web.bean.MsgBean;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class AuthTokenServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        /*
         * 响应头：json
         */
        resp.setContentType("text/json;charset=UTF-8");
        resp.setCharacterEncoding("UTF-8");
        PrintWriter out = resp.getWriter();
        String token = req.getHeader("Authorization");
        MsgBean msg = new MsgBean();
        // token不存在
        if (token == null || token.length() <= 0) {
            msg.setStatus(-1);
        }
        // token存在
        else {
            String uid = JWT.decode(token).getClaim("aud").asString();

            // 简单验证
            if (uid != null && uid.length() > 0) {
                msg.setStatus(1);
            } else {
                msg.setStatus(-1);
            }
        }

        /*
         * 复杂验证
         * 去tuser表中看loginname中是否存在uid
         */


        Gson gson = new Gson();

        out.println(gson.toJson(msg));
        out.flush();
        out.close();

    }
}

