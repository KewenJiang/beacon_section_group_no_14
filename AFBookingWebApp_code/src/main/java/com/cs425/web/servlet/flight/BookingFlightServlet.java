package com.cs425.web.servlet.flight;

import com.cs425.web.bean.CustomerBean;
import com.cs425.web.bean.FlightBean;
import com.cs425.web.bean.MsgBean;
import com.cs425.web.exception.AppException;
import com.cs425.web.service.CustomerService;
import com.cs425.web.service.FlightService;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import static java.lang.Double.parseDouble;
import static java.lang.Integer.parseInt;

public class BookingFlightServlet extends HttpServlet{
    private static final long serialVersionUID = 1L;

    public BookingFlightServlet(){
        super();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        /*
         * 响应头：json
         */
        resp.setContentType("text/json;charset=UTF-8");
        resp.setCharacterEncoding("UTF-8");
        PrintWriter out = resp.getWriter();

        String airlineId=req.getParameter("airlineId");
        String flightNumber=req.getParameter("flightNumber");
        String flightData=req.getParameter("flightData");
        String  departureAirport=req.getParameter("departureAirport");
        String  destinationAirport=req.getParameter("destinationAirport");
        String  departureTime=req.getParameter("departureTime");
        String  destinationTime=req.getParameter("departureAirport");
        String  flightLength1=req.getParameter("flightLength");
        String  firstClassSeats1=req.getParameter("firstClassSeats");
        String  economyClassSeats1=req.getParameter("economyClassSeats");
        String  firstClassPrice1=req.getParameter("firstClassPrice");
        String  economyClassPrice1=req.getParameter("economyClassPrice");

        Integer  flightLength = parseInt(flightLength1);
        Integer  firstClassSeats = parseInt(firstClassSeats1);
        Integer  economyClassSeats = parseInt(economyClassSeats1);
        Double   firstClassPrice = parseDouble(firstClassPrice1);
        Double   economyClassPrice = parseDouble(economyClassPrice1);


        FlightBean flb=new FlightBean(airlineId, flightNumber, flightData, departureAirport, destinationAirport, departureTime, destinationTime, flightLength, firstClassSeats, economyClassSeats, firstClassPrice,economyClassPrice);
        Gson gson=new Gson();
        MsgBean msg=new MsgBean();

        FlightService fl=new FlightService();
        try {
            //预订成功
            if(fl.BookFlight(flb)>0) {
                msg.setStatus(1);
            }
            //预订失败
            else {
                msg.setStatus(-1);
                msg.setErrmsg("预订失败!");
            }
        } catch (AppException e) {
            // TODO Auto-generated catch block
            msg.setStatus(-1);
            msg.setErrmsg(e.getErrMsg());
        }

        out.println(gson.toJson(msg));
        out.flush();
        out.close();
    }
}
