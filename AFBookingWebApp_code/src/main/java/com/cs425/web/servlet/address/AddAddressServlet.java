package com.cs425.web.servlet.address;

import com.cs425.web.bean.AddressBean;

import com.cs425.web.bean.MsgBean;
import com.cs425.web.exception.AppException;
import com.cs425.web.service.AddressService;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

import static java.lang.Integer.parseInt;

public class AddAddressServlet extends HttpServlet{
    private static final long serialVersionUID = 1L;

    public AddAddressServlet(){
        super();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        /*
         * 响应头：json
         */
        resp.setContentType("text/json;charset=UTF-8");
        resp.setCharacterEncoding("UTF-8");
        PrintWriter out = resp.getWriter();

        String state=req.getParameter("state");
        String city=req.getParameter("city");
        String area=req.getParameter("area");
        String  streetName=req.getParameter("streetName");
        String  aptName=req.getParameter("aptName");
        String  houseNumber=req.getParameter("houseNumber");
        String  postalCode=req.getParameter("postalCode");
        String  emailAddress=req.getParameter("emailAddress");

        AddressBean address=new AddressBean(state, city, area,streetName,aptName,houseNumber,parseInt(postalCode),emailAddress);
        Gson gson=new Gson();
        MsgBean msg=new MsgBean();
        AddressService ads=new AddressService();
        try {
            //新增成功
            if(ads.addAddress(address)>0) {
                msg.setStatus(1);
            }
            //新增失败
            else {
                msg.setStatus(-1);
                msg.setErrmsg("数据新增失败!");
            }
        } catch (AppException e) {
            // TODO Auto-generated catch block
            msg.setStatus(-1);
            msg.setErrmsg(e.getErrMsg());
        }

        out.println(gson.toJson(msg));
        out.flush();
        out.close();
    }
}
