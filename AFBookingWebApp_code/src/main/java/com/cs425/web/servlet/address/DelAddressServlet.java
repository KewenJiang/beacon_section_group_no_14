package com.cs425.web.servlet.address;

import com.cs425.web.bean.MsgBean;
import com.cs425.web.exception.AppException;
import com.cs425.web.service.AddressService;
import com.cs425.web.service.CreditCardsService;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

import static java.lang.Integer.parseInt;

public class DelAddressServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        /*
         * 响应头：json
         */
        resp.setContentType("text/json;charset=UTF-8");
        resp.setCharacterEncoding("UTF-8");
        PrintWriter out = resp.getWriter();
        String email = req.getParameter("emailAddress");
        String code = req.getParameter("postalCode");
        Integer postalCode = parseInt(code);



        AddressService ads = new AddressService();
        Gson gson = new Gson();
        MsgBean msg = new MsgBean();


            try {
                //删除成功
                if (ads.deleteAddress(email,postalCode) > 0 ) {
                    msg.setStatus(1);
                }
                //删除失败
                else {
                    msg.setStatus(-1);
                    msg.setErrmsg("数据删除失败!");
                }
            } catch (AppException e) {
                // TODO Auto-generated catch block
                msg.setStatus(-1);
                msg.setErrmsg(e.getErrMsg());
            }

            out.println(gson.toJson(msg));
            out.flush();
            out.close();
        }
}
