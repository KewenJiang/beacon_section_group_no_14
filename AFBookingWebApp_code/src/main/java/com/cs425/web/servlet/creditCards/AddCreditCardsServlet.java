package com.cs425.web.servlet.creditCards;
import com.cs425.web.bean.CreditCardsBean;
import com.cs425.web.bean.CustomerBean;

import com.cs425.web.bean.MsgBean;
import com.cs425.web.exception.AppException;
import com.cs425.web.service.CreditCardsService;
import com.cs425.web.service.CustomerService;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AddCreditCardsServlet extends HttpServlet{
    private static final long serialVersionUID = 1L;

    public AddCreditCardsServlet(){
        super();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        /*
         * 响应头：json
         */
        resp.setContentType("text/json;charset=UTF-8");
        resp.setCharacterEncoding("UTF-8");
        PrintWriter out = resp.getWriter();

        String cardNumber=req.getParameter("cardNumber");
        String bankName=req.getParameter("bankName");
        String addressId=req.getParameter("addressId");
        String  emailAddress=req.getParameter("emailAddress");

        CreditCardsBean cc=new CreditCardsBean(cardNumber, bankName, addressId,emailAddress);
        Gson gson=new Gson();
        MsgBean msg=new MsgBean();

        CreditCardsService ccs=new CreditCardsService();
        try {
            //新增成功
            if(ccs.addCreditCards(cc)>0) {
                msg.setStatus(1);
            }
            //新增失败
            else {
                msg.setStatus(-1);
                msg.setErrmsg("数据新增失败!");
            }
        } catch (AppException e) {
            // TODO Auto-generated catch block
            msg.setStatus(-1);
            msg.setErrmsg(e.getErrMsg());
        }

        out.println(gson.toJson(msg));
        out.flush();
        out.close();
    }
}
