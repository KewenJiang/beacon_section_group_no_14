package com.cs425.web.servlet.customer;

import com.cs425.web.bean.CustomerBean;
import com.cs425.web.bean.MsgBean;
import com.cs425.web.exception.AppException;
import com.cs425.web.service.AddressService;
import com.cs425.web.service.CreditCardsService;
import com.cs425.web.service.CustomerService;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

public class UpdateCustomerServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        /*
         * 响应头：json
         */
        resp.setContentType("text/json;charset=UTF-8");
        resp.setCharacterEncoding("UTF-8");
        PrintWriter out = resp.getWriter();
        String email=req.getParameter("emailAddress");
        String customerName=req.getParameter("customerName");
        String phoneNumber=req.getParameter("phoneNumber");
        String  iata=req.getParameter("IATA");
        String  state=req.getParameter("state");
        String  city=req.getParameter("city");
        String  area=req.getParameter("area");
        String  streetName=req.getParameter("streetName");
        String  aptName=req.getParameter("aptName");
        String  houseNumber=req.getParameter("houseNumber");
        //将Date转换为String
        Date currentTime = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateString = formatter.format(currentTime);
        CustomerBean customer=new CustomerBean(email, customerName, phoneNumber, dateString,iata);
        CustomerService cs=new CustomerService();
        AddressService as = new AddressService();
        CreditCardsService ccs = new CreditCardsService();
        Gson gson=new Gson();
        MsgBean msg=new MsgBean();

        try {
            //修改成功
            if(cs.updateCustomer(customer)>0) {
                msg.setStatus(1);
            }
            //修改失败
            else {
                msg.setStatus(-1);
                msg.setErrmsg("数据修改失败!");
            }
        } catch (AppException e) {
            // TODO Auto-generated catch block
            msg.setStatus(-1);
            msg.setErrmsg(e.getErrMsg());
        }

        out.println(gson.toJson(msg));
        out.flush();
        out.close();
    }
}
