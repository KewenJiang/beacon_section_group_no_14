package com.cs425.web.servlet.creditCards;

import com.cs425.web.exception.AppException;
import com.cs425.web.service.CreditCardsService;
import com.cs425.web.service.CustomerService;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class getCreditCardsServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        /*
         * 响应头：json
         */
        resp.setContentType("text/json;charset=UTF-8");
        resp.setCharacterEncoding("UTF-8");
        PrintWriter out = resp.getWriter();

        String email=req.getParameter("emailAddress");
        String cardNumber=req.getParameter("cardNumber");

        System.out.println("===="+email+"===="+cardNumber);


        CreditCardsService ccs=new CreditCardsService();

        Gson gson=new Gson();


        try {
            out.println(gson.toJson(ccs.getOne(email,cardNumber)));
        } catch (NumberFormatException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (AppException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        out.flush();
        out.close();

    }
}

