package com.cs425.web.servlet.airline;

import com.cs425.web.bean.AddressBean;
import com.cs425.web.bean.AirlineBean;
import com.cs425.web.bean.MsgBean;
import com.cs425.web.exception.AppException;
import com.cs425.web.service.AddressService;
import com.cs425.web.service.AirLineService;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

import static java.lang.Integer.parseInt;

public class AddAirlineServlet extends HttpServlet{
    private static final long serialVersionUID = 1L;

    public AddAirlineServlet(){
        super();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        /*
         * 响应头：json
         */
        resp.setContentType("text/json;charset=UTF-8");
        resp.setCharacterEncoding("UTF-8");
        PrintWriter out = resp.getWriter();

        String airlineId=req.getParameter("airlineId");
        String airlineName=req.getParameter("airlineName");
        String originCuntry=req.getParameter("originCuntry");

        AirlineBean airline=new AirlineBean(airlineId, airlineName, originCuntry);
        Gson gson=new Gson();
        MsgBean msg=new MsgBean();
        AirLineService als=new AirLineService();
        try {
            //新增成功
            if(als.addAirline(airline)>0) {
                msg.setStatus(1);
            }
            //新增失败
            else {
                msg.setStatus(-1);
                msg.setErrmsg("数据新增失败!");
            }
        } catch (AppException e) {
            // TODO Auto-generated catch block
            msg.setStatus(-1);
            msg.setErrmsg(e.getErrMsg());
        }

        out.println(gson.toJson(msg));
        out.flush();
        out.close();
    }
}
