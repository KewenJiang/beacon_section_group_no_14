package com.cs425.web.servlet.customer;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.cs425.web.bean.CustomerBean;
import com.cs425.web.bean.MsgBean;
import com.cs425.web.exception.AppException;
import com.cs425.web.service.CustomerService;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class LoginServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        /*
         * 响应头：json
         */
        resp.setContentType("text/json;charset=UTF-8");
        resp.setCharacterEncoding("UTF-8");
        PrintWriter out = resp.getWriter();
        String data = "";
        String email = req.getParameter("emailAddress");
        String phoneNumber = req.getParameter("phoneNumber");
        MsgBean msg = new MsgBean();

        CustomerService cs = new CustomerService();
        CustomerBean customer;
        try {
            customer = cs.chkLogin(email, phoneNumber);

            // 登录成功
            msg.setStatus(1);


            /*
             * 登录成功 利用jwt生成Token
             *
             */

            String token= JWT.create().withAudience(email).sign(Algorithm.HMAC256(customer.getPhoneNumber()));



            System.out.println("token============="+token);

            msg.setErrmsg(token);


        } catch (AppException e) {
            // TODO Auto-generated catch block

            msg.setStatus(-1);
            msg.setErrmsg(e.getErrMsg());

        }

        //写到前端
        Gson gson = new Gson();
        out.println(gson.toJson(msg));
        out.flush();
        out.close();
    }
}
