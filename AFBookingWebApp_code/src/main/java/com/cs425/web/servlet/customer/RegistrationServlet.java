package com.cs425.web.servlet.customer;

import com.cs425.web.bean.CustomerBean;
import com.cs425.web.bean.MsgBean;
import com.cs425.web.exception.AppException;
import com.cs425.web.service.CustomerService;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

public class RegistrationServlet extends HttpServlet{
    private static final long serialVersionUID = 1L;

    public RegistrationServlet(){
        super();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        /*
         * 响应头：json
         */
        resp.setContentType("text/json;charset=UTF-8");
        resp.setCharacterEncoding("UTF-8");
        PrintWriter out = resp.getWriter();

        String emailAddress=req.getParameter("email");
        String name=req.getParameter("name");
        String phoneNumber=req.getParameter("phoneNumber");
        String  IATA=req.getParameter("IATA");
        //将Date转换为String
        Date currentTime = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateString = formatter.format(currentTime);


        CustomerBean cst=new CustomerBean(emailAddress, name, phoneNumber ,dateString, IATA);
        Gson gson=new Gson();
        MsgBean msg=new MsgBean();

        CustomerService cs=new CustomerService();
        try {
            //新增成功
            if(cs.addCustomer(cst)>0) {
                msg.setStatus(1);
            }
            //新增失败
            else {
                msg.setStatus(-1);
                msg.setErrmsg("注册新增失败!");
            }
        } catch (AppException e) {
            // TODO Auto-generated catch block
            msg.setStatus(-1);
            msg.setErrmsg(e.getErrMsg());
        }

        out.println(gson.toJson(msg));
        out.flush();
        out.close();
    }
}
